﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcAccommodation.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to Ekutileni Lodge, your African getaway.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your peaceful African getaway.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "We may be a retreat but you can still contact us.";

            return View();
        }
    }
}
